#!/usr/bin/python3
try:
    import sys, os, subprocess, shutil, requests, pyperclip
    from colorama import Fore, Back, Style
except ImportError: # most common issue reported from testers
    print('Missing libraries! Run this command:\n'
            + '"pip3 install requests pyperclip colorama --user"')
    quit()
url = 'https://invidio.us/' # it is always a good idea to avoid hard coding
print(Fore.GREEN + 'Invex - Invidio.us URL grabber')

# if arguments are given, use that. If not, ask for input.
if len(sys.argv) > 1:
    args = ' '.join(sys.argv[1:])
    isq = args.replace(' ','+')
else:
    print(Style.NORMAL + '\nInput search query:')
    isq = input(' > ').replace(' ','+')

# print search query url
sq = url+'search?q='+isq
print(Style.DIM + sq + Style.RESET_ALL)

# get api
api = url+'api/v1/search?q='+isq
try:
    j = requests.get(api).json()
except requests.exceptions.ConnectionError: # no internet/invidious is down
    print(Fore.RED + Style.BRIGHT
            + 'cannot connect to "' + url + '", please try again later.')
    quit()

# declare variables for the search result loop
num = [] # reference number to the URL on the printed list
title = [] # title of the reference
k = -1 # reference number
kprefx = ' ' # to ensure that the results are displayed evenly

# loop through results
for i in j:
    if k < 9:
        kprefx = ' '
    else:
        kprefx = ''
    k+=1
    print(Style.BRIGHT + Fore.GREEN + kprefx + str(k) + Style.DIM + '|'
            + Style.RESET_ALL + ' URL: ' + Style.BRIGHT + Fore.RED 
            + url + 'watch?v=%s' % (i['videoId']) + Style.RESET_ALL
            + ' - Title: %s' % (i['title']))
    # index the URLs and titles to a number that can be called on later
    num.append(str(url + 'watch?v=%s' % (i['videoId'])))
    title.append(str(i['title']))

# final prompt
while True:
    print(Fore.GREEN + 'Pick a number to copy URL, or run number with mpv or ydl:')
    print(Fore.GREEN + 'Or type quit to quit')
    pick = input(' > ')
    try:
        if os.environ['XDG_SESSION_TYPE'] == 'wayland':
            try:
                subprocess.run([shutil.which('wl-copy'),num[int(pick)]])
                print('copied "'+ (title[int(pick)]) +'" to the clipboard!')
            except FileNotFoundError:
                print('Install "wl-clipboard" to use this feature. Run:\n'
                        +'sudo apt install wl-clipboard')
                quit()
        else:
            pyperclip.copy(num[int(pick)])
            print('copied "'+ (title[int(pick)]) +'" to the clipboard!')
    except (ValueError, IndexError) as e:
        if 'mpv' in pick or 'vlc' in pick:
            choice = pick.split(" ")[0]
            print('loading "'+ (title[int(pick.replace(choice,''))]) +'"')
            subprocess.run([shutil.which(choice),num[int(pick.replace(choice,''))]])
        elif 'ydl' in pick or 'youtube-dl' in pick:
            print('downloading "'+ (title[int(pick.replace('ydl',''))]) +'"')
            subprocess.run([shutil.which('youtube-dl'),num[int(pick.replace('ydl',''))]])
        elif 'quit' in pick:
            quit()
        else:
            print(Fore.RED + 'Invalid command -- try "0", "mpv 0", "vlc 0", "ydl 0", or'
            + ' "youtube-dl 0".\nAlso, verify that the number is between 0-19.' + Fore.GREEN)
